export const bots = [
    {
      name: 'starter',
      startingMoney: 10,
      mps: 1,
      numOfBots: 0,
    },
    {
      name: 'fresh',
      startingMoney: 100,
      mps: 2,
      numOfBots: 0,
    },
    {
      name: 'middle',
      startingMoney: 1000,
      mps: 5,
      numOfBots: 0,
    },
    {
      name: 'high',
      startingMoney: 10000,
      mps: 10,
      numOfBots: 0,
    },
  ];