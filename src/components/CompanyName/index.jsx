import { useState } from "react";

export const CompanyName = props => {
  const { handleOnClick } = props;
  const [value, setValue] = useState("");

  return (
    <div>
      <h1>Welcome to Money Maker!</h1>
      <h3>Enter name below:</h3>
      <input
        type="text"
        value={value}
        onChange={event => setValue(event.target.value)}
      />
      <button
        onClick={() => {
          handleOnClick(value);
        }}
      >
        Go!
      </button>
    </div>
  );
};
