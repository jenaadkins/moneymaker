import { useContext } from "react";
import { MoneyContext } from "../../MoneyContext";
import add from "../../assets/add.svg";

export const Bot = props => {
  const { name = "", startingMoney, mps, numOfBots, handleOnBotAdd } = props;
  const currentMoney = useContext(MoneyContext);
  console.log(currentMoney);
  const currentPrice =
    numOfBots === 0
      ? startingMoney
      : Math.ceil(startingMoney * (numOfBots * 1.1) + startingMoney);

  return (
    <button
      disabled={currentPrice > currentMoney}
      onClick={() => handleOnBotAdd(name, currentPrice)}
    >
      <span className="bot-name">{name}</span>
      <span>Cost: ${currentPrice}</span>
      <span>Bots: {numOfBots}</span>
      <span>Current $/ps: {numOfBots ? numOfBots * mps : 0}</span>
      <span>${mps}/per second/per Bot</span>

      <img width="100px " height="150px" src={add} alt="add bot" />
      <a href="http://google.com" target="_blank">
        Your Link Here
      </a>
    </button>
  );
};
