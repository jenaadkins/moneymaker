import logo from "./logo.svg";
import "./App.scss";
import { useState, useEffect, useCallback } from "react";
import { bots as Bots } from "./bots";
import { Bot } from "./components/Bot";
import { MoneyContext } from "./MoneyContext";
import { useOnMount } from "./useOnMount";
import { CompanyName } from "./components/CompanyName";

function App() {
  const [money, setMoney] = useState(0);
  const [bots, setBots] = useState(Bots);
  const [isGameStateLoaded, setIsGameStateLoaded] = useState(false);
  const [companyName, setCompanyName] = useState("");

  const handleCompanyName = name => {
    setCompanyName(name);
  };

  /**
   * Handles the setting of money after buying a new bot.
   */
  const handleOnClick = () => {
    setMoney(currentMoney => {
      return currentMoney + 1;
    });
  };

  /**
   * Returns the sum of the money/ps.
   */
  const getMPSCalculation = useCallback(() => {
    let mps = 0;

    bots.forEach(bot => {
      mps += bot.mps * bot.numOfBots;
    });

    return mps;
  }, [bots]);

  /**
   * Handle adding a new bot.
   * @param {*} name - Name of the bot
   * @param {*} currentPrice The current price of the bot
   */
  const handleOnBotAdd = (name, currentPrice) => {
    //* Gets the index of the bot we want to add.
    let botIndex = bots.findIndex(bot => {
      return bot.name === name;
    });

    //* If botIndex exists, add +1 to numOfBots and reduce money by currentPrice
    if (botIndex !== -1) {
      let newBots = bots.slice();
      newBots[botIndex].numOfBots += 1;
      setBots(newBots);
      setMoney(money => {
        return money - currentPrice;
      });
    }
  };

  /**
   * Saves the game state to `localStorage`
   */
  const handleOnSave = currentMoney => {
    let saveObject = {
      currentMoney: currentMoney,
      bots: bots,
      currentTime: new Date().getTime()
    };

    localStorage.setItem("moneyMaker", JSON.stringify(saveObject));
  };

  /**
   * Loads the game state if is exists in `localStorage`
   */
  const handleOnLoad = () => {
    let moneyMaker = getMoneyMakerStorage();

    setBots(moneyMaker.bots);
    setIsGameStateLoaded(true);
  };

  const getMoneyMakerStorage = () => {
    let moneyMaker =
      localStorage.getItem("moneyMaker") ||
      JSON.stringify({
        currentMoney: money || 1,
        bots: bots,
        currentTime: new Date().getTime()
      });

    try {
      moneyMaker = JSON.parse(moneyMaker);
    } catch (e) {
      alert("Could not parse the data.");
    }

    return moneyMaker;
  };

  useEffect(() => {
    let moneyMaker = getMoneyMakerStorage();
    let currentMoney = moneyMaker.currentMoney;
    let currentTime = new Date().getTime();
    let timeDifference = Math.floor(
      (currentTime - moneyMaker.currentTime) / 1000
    );
    currentMoney += getMPSCalculation() * timeDifference;

    setMoney(moneyMaker.currentMoney);
  }, [isGameStateLoaded]);

  useEffect(() => {
    handleOnLoad();
  }, []);

  useEffect(() => {
    let interval = setInterval(() => {
      let addedMoney = +getMPSCalculation().toFixed(2);
      setMoney(money => {
        const NEW_MONEY = money + addedMoney;
        handleOnSave(NEW_MONEY);
        return NEW_MONEY;
      });
    }, 1000);
    return () => clearInterval(interval);
  }, [getMPSCalculation]);

  if (!companyName) {
    return <CompanyName handleOnClick={handleCompanyName} />;
  }
  return (
    <div className="container">
      <div className="left-container">
        <h1>{companyName}</h1>
        <div className="bot-container">
          <MoneyContext.Provider value={money}>
            {bots.map((bot, index) => {
              return (
                <Bot
                  key={bot.name}
                  name={bot.name}
                  startingMoney={bot.startingMoney}
                  mps={bot.mps}
                  numOfBots={bot.numOfBots}
                  handleOnBotAdd={handleOnBotAdd}
                />
              );
            })}
          </MoneyContext.Provider>
        </div>
      </div>
      <div className="right-container">
        <h1>${money}</h1>
        <span> Current $/ps: {getMPSCalculation()}</span>
        <button
          onClick={() => {
            handleOnClick();
          }}
        >
          +$1
        </button>

        <button onClick={() => handleOnSave(money)}>Save</button>
      </div>
    </div>
  );
}

export default App;

//TODO: need variable to track amount of money
//TODO: a button that adds $1 to our money
//TODO: have a variable that tracks the amount of current $/ps
//TODO: 4 bots that add money automatically
//TODO: everytime a new bot is added the price of that bot increases by some percentage
//TODO: -----Bots-----
//TODO: name, initial cost, $/ps
//TODO: -----Display-----
//TODO: name, current cost, num of bots, current $/ps/bot
//TODO: disable bot if not enough money to buy it
