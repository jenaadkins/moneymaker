import { useEffect, useCallback } from "react";

export const useOnMount = mountFunction => {
  let mountFunctionCallback = useCallback(mountFunction, [mountFunction]);
  useEffect(() => {
    mountFunctionCallback();
  }, [mountFunctionCallback]);
};
